$(document).ready(function() {
    $(".contact-us-link").click(function() {
        $(this).hide();
        $(".remove-contact-us").show();
        $(".contact-us-sidebar-wrapper").removeClass("shrinked1").css({"z-index": "999", "transform": "scale(1, 1) !important"});
        $(".contact-us-sidebar-wrapper .sidebar-inner").removeClass("shrinked").css({"transform": "scale(1, 1) !important"});
        $(".main-contents-section").css({"left": "-390px"});
    });
    $(".remove-contact-us").click(function() {
        setTimeout(
                function() {
                    $(".remove-contact-us").hide();
                },
                1000);
        setTimeout(
                function() {
                    $(".contact-us-link").delay(4000).show();
                },
                1000);

//        $(".contact-us-link").delay(4000).show();
        $(".contact-us-sidebar-wrapper").addClass("shrinked1").css({"z-index": "-11"});
        $(".contact-us-sidebar-wrapper .sidebar-inner").addClass("shrinked");
        $(".main-contents-section").css({"left": "0", "z-index": "999"});
    });


    /*--  Handling Scroll To Top Button Event Starting  --*/
    jQuery(window).scroll(function() {
        if (jQuery(window).scrollTop() > 50) {
            jQuery("#back-to-top").fadeIn(200);
        } else {
            jQuery("#back-to-top").fadeOut(200);
        }
    });
    jQuery('#back-to-top, .back-to-top').click(function() {
        jQuery('html, body').animate({scrollTop: 0}, '800');
        return false;
    });

    //smooth scroll to href value
    jQuery("#top-menu li a , .page-up-btn").click(function(event) {       
        //calculate destination place       
        var dest = 0;
        if ($(this.hash).offset().top > $(document).height() - $(window).height()) {
            dest = $(document).height() - $(window).height();
        } else {
            dest = $(this.hash).offset().top;
        }
        //go to destination
        jQuery('html,body').animate({scrollTop: dest}, 1000, 'swing');
         event.preventDefault();
    });

    /*--  Handling Scroll To Top Button Event Ending  --*/
//timeline plugin js
    var ev = [{
            id: 1,
            name: "Joined Faecbook",
            on: new Date(2011, 2, 15)
        }, {
            id: 11,
            name: "Updated my first status message",
            on: new Date(2011, 2, 17)
        }, {
            id: 2,
            name: "Joined Twitter",
            on: new Date(2011, 5, 30)
        }, {
            id: 9,
            name: "Created a new blogger account",
            on: new Date(2011, 7, 5)
        }, {
            id: 3,
            name: "Trip to Australia",
            on: new Date(2012, 5, 5)
        }, {
            id: 4,
            name: "Trip to New Zealand",
            on: new Date(2012, 5, 30)
        }, {
            id: 5,
            name: "Awesome new year",
            on: new Date(2013, 0, 1)
        }, {
            id: 6,
            name: "Will go to Moon",
            on: new Date(2013, 6, 10)
        }, {
            id: 7,
            name: "Will go to Mars",
            on: new Date(2014, 6, 10)
        }, {
            id: 8,
            name: "No idea about this date",
            on: new Date(2015, 6, 10)
        }]
    var tl = $('#myTimeline').jqtimeline({
        events: ev,
        numYears: 4,
        startYear: 2011,
        click: function(e, event) {
            alert(event.name);
        }
    });
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();



//portfolio down js
//
// All images need to be loaded for this plugin to work so
// we end up waiting for the whole window to load in this example
    $(window).load(function() {
        $(document).ready(function() {
            collage();
            $('.Collage').collageCaption();
        });
    });


// Here we apply the actual CollagePlus plugin
    function collage() {
        $('.Collage').removeWhitespace().collagePlus(
                {
                    'fadeSpeed': 2000,
                    'targetHeight': 300,
                    'effect': 'effect-2',
                    'direction': 'vertical'
                }
        );
    }
    ;

// This is just for the case that the browser window is resized
    var resizeTimer = null;
    $(window).bind('resize', function() {
        // hide all the images until we resize them
        $('.Collage .Image_Wrapper').css("opacity", 1);
        // set a timer to re-apply the plugin
        if (resizeTimer)
            clearTimeout(resizeTimer);
        resizeTimer = setTimeout(collage, 200);
    });

    if (Modernizr.touch) {
        // show the close overlay button
        $(".close-overlay").removeClass("hidden");
        // handle the adding of hover class when clicked
        $(".img").click(function(e) {
            if (!$(this).hasClass("hover")) {
                $(this).addClass("hover");
            }
        });
        // handle the closing of the overlay
        $(".close-overlay").click(function(e) {
            e.preventDefault();
            e.stopPropagation();
            if ($(this).closest(".img").hasClass("hover")) {
                $(this).closest(".img").removeClass("hover");
            }
        });
    } else {
        // handle the mouseenter functionality
        $(".img").mouseenter(function() {
            $(this).addClass("hover");
        })
                // handle the mouseleave functionality
                .mouseleave(function() {
                    $(this).removeClass("hover");
                });
    }




    //Check to see if the window is top if not then display button
    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('#topcontrol').fadeIn();
            $('.arrow-down-link').fadeIn();
        } else {
            $('#topcontrol').fadeOut();
            $('.arrow-down-link').fadeOut();
        }
    });

    //Click event to scroll to top
    $('#topcontrol').click(function() {
        $('html, body').animate({scrollTop: 0}, 5000);
        return false;
    });
//    $('.arrow-down-link').click(function() {
//        $('html, body').animate({scrollTop: 0}, 1000);
//        return false;
//    });



    $(function() {

        // Instantiate MixItUp:

        $('#Container').mixItUp();

    });
});

// Scroll Top to Bottom and Bottom to Top
$(function() {
	// the element inside of which we want to scroll
        var $elem = $('#top');

        // show the buttons
	$('#nav_up').fadeIn('slow');
	$('#nav_down').fadeIn('slow');  

        // whenever we scroll fade out both buttons
	$(window).bind('scrollstart', function(){
		$('#nav_up,#nav_down').stop().animate({'opacity':'0.2'});
	});
        // ... and whenever we stop scrolling fade in both buttons
	$(window).bind('scrollstop', function(){
		$('#nav_up,#nav_down').stop().animate({'opacity':'1'});
	});

        // clicking the "down" button will make the page scroll to the $elem's height
	$('#nav_down').click(
		function (e) {
                    //For at the bottom
                    //$('html, body').animate({scrollTop: $elem.height()}, 800);
                    //For at the bottom given height
                    $("html, body").animate({ scrollTop: $(window).height()+200}, 600);
		}
	);
        // clicking the "up" button will make the page scroll to the top of the page
	$('#nav_up').click(
		function (e) {
			$('html, body').animate({scrollTop: '0px'}, 800);
		}
	);
 });

