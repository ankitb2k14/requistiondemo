import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="header-wrapper">
          <nav className="navbar navbar-default ">
            <div className="container-fluid">
              <div className="navbar-header">
                <button type="button" className="btn btn-link btn-lg navbar-toggle collapsed visible-lg-inline-block pull-left" data-target="#bs-example-navbar-collapse-1"
                  aria-expanded="false">
                  <span className="sr-only">Toggle navigation</span>
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                </button>
                <a className="navbar-brand" href="#">
                  <img src="images/logo.jpg" />
                </a>
              </div>

              <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul className="nav navbar-nav navbar-right">
                  <li><a href="#">Log out</a></li>
                </ul>
              </div>
            </div>
          </nav>
        </header>
        <main className="main-wrapper">
          <div className="panel panel-default custom-panel">
            <div className="panel-heading">
              <h3 className="panel-title">Create Requisition <a href="javascript:void(0);">Back to dashboard</a></h3>
            </div>
            <div className="panel-body">
              <div className="row">
                <div className="col-sm-9">
                  <form>
                    <p className="h5 py-4">Search or Enter job code</p>

                    <div className="row">
                      <div className="col-sm-5">
                        <div className="md-form">
                          <input type="text" id="materialFormCardNameEx" className="form-control" placeholder="Start typing job or title to filter" />
                          <label htmlFor="materialFormCardNameEx" className="font-weight-light">Job Title</label>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-sm-4">
                        <div className="md-form">
                          <select className="mdb-select md-form">
                            <option value="" disabled defaultValue>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                          </select>
                          <label htmlFor="materialFormCardEmailEx" className="font-weight-light">Division</label>
                        </div>
                      </div>
                      <div className="col-sm-4">
                        <div className="md-form">
                          <select className="mdb-select md-form">
                            <option value="" disabled defaultValue>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                          </select>
                          <label htmlFor="materialFormCardEmailEx" className="font-weight-light">Division</label>
                        </div>
                      </div>
                      <div className="col-sm-4">

                        <div className="md-form">
                          <select className="mdb-select md-form">
                            <option value="" disabled defaultValue>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                          </select>
                          <label htmlFor="materialFormCardEmailEx" className="font-weight-light">Division</label>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-sm-12">
                        <div className="md-form">
                          <input type="text" id="materialFormCardNameEx" className="form-control" placeholder="Start typing job or title to filter" />
                          <label htmlFor="materialFormCardNameEx" className="font-weight-light">Job Title</label>
                        </div>
                      </div>
                    </div>

                    <p className="h5 py-4">Requisition Details</p>

                    <div className="row">
                      <div className="col-sm-4">
                        <div className="md-form">
                          <select className="mdb-select md-form">
                            <option value="" disabled defaultValue>Choose your option</option>
                            <option value="1">Part Time</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                          </select>
                          <label htmlFor="materialFormCardEmailEx" className="font-weight-light">Select
                                            Environment Type</label>
                        </div>
                      </div>
                      <div className="col-sm-4">
                        <p>Select to see current application for the job code entered above</p>
                        <a href="javascript:void(0);" className="text-info">Potential Applicant Pool</a>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-sm-4">
                        <div className="md-form">
                          <select className="mdb-select md-form">
                            <option value="" disabled defaultValue>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                          </select>
                          <label htmlFor="materialFormCardEmailEx" className="font-weight-light">Select Number of
                                            Position</label>
                        </div>
                      </div>
                      <div className="col-sm-4">

                        <div className="md-form">
                          <select className="mdb-select md-form">
                            <option value="" disabled defaultValue>Choose your option</option>
                            <option value="1">John Smith</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                          </select>
                          <label htmlFor="materialFormCardEmailEx" className="font-weight-light">Hiring Manager</label>
                        </div>
                      </div>
                      <div className="col-sm-4">
                        <div className="md-form">
                          <select className="mdb-select md-form">
                            <option value="" disabled defaultValue>Choose your option</option>
                            <option value="1"> John Smith</option> <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                          </select>
                          <label htmlFor="materialFormCardEmailEx" className="font-weight-light">Reason for
                                            Creation</label>
                        </div>
                      </div>
                    </div>

                    <div className="text-center py-4 mt-3">
                      <button className="btn btn-cyan" type="submit">Create</button>
                    </div>
                  </form>
                </div>
                <div className="col-sm-3">
                  <div className="panel panel-info">
                    <div className="panel-heading">
                      <h3 className="panel-title">
                        <img className="img-respon" src="images/star-header.jpg" alt="start" />
                        Applicant Pool(20)
                                </h3>
                    </div>
                    <div className="panel-body">
                      <ul className="list-unstyled">
                        <li>Internal Candidates <span className="count">10</span></li>
                        <li>Excellent Fit <span className="count">5</span></li>
                        <li>Good Fit <span className="count">5</span></li>
                      </ul>
                      <ul className="list-unstyled">
                        <li>External Candidates <span className="count">10</span></li>
                        <li>Excellent Fit <span className="count">5</span></li>
                        <li>Good Fit <span className="count">5</span></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <footer className="footer-wrapper">
          <div className="container-fluid">
            <div className="row">
              <div className="col-xs-6">
                <p className="copyright">&copy; 2018 Walmart</p>
              </div>
              <div className="col-xs-6">
                <ul className="list-inline">
                  <li><img className="img-responsive" src="images/star.jpg" alt="star" /></li>
                  <li><img className="img-responsive" src="images/footer-logo.jpg" alt="footer-logo" /></li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}

export default App;
